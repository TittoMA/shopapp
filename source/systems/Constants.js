import { FormatString } from '../libraries/Format';

const DefaultTheme = {
    AppBackgroundColor: '#eeeeee', // Main BG color
    AppHeaderBackgroundColor: '#2b78e4',
    AppSecondaryBackgroundColor: '#fafafa', // Secondary BG color
    AppBottomTabBackgroundColor: '#f7f7f7',
    AppPrimaryColor: '#2b78e4', // Header Background
    AppMainButtonColor: '#2b78e4',

    AppStatusBarColor: '#ed5564', // 
    AppStatusBarStyle: 'light-content',

    AppObjectSpacing: 8, // Spacing
    AppInputHeightDefault: 48,

    AppFontFace: 'Poppins', // Main Font Familiy
    AppFontBlackColor: '#444444',
    AppFontGreyColor: '#777777',
    AppFontLightColor: '#ffffff',
    AppFontHeadingColor: '#6a89cc',
    AppFontParagraphColor: '#999999',
    AppFontGreenColor: "#00CC64",
    AppHeaderFontColor: '#ffffff',
}

export const AppString = {
    
}

export const AppNumber = {
    
}

export const AppConstants = {
    ActiveTheme: DefaultTheme,
}

export const AppStorageKey = {
    
}