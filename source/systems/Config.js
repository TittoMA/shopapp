import React from 'react'
import { AsyncStorage } from 'react-native'

import { createBottomTabNavigator, createTabNavigator, createStackNavigator, createAppContainer } from 'react-navigation'

// App Screen
import LoginScreen from '../controllers/LoginScreen'
import RegisterScreen from '../controllers/RegisterScreen'

// ********************************************************************************
// * APP Config
// ********************************************************************************

export const AppConfig = {
}

// ********************************************************************************
// * Main Drawer Navigator
// ********************************************************************************

export const MainDrawer = (auth) => {
    return createAppContainer(
        createStackNavigator({

            Login: {
                screen: LoginScreen
            },
            Register: {
                screen: RegisterScreen
            },
        }, {
                headerMode: 'none',
                disableOpenGesture: true,
                initialRouteName: "Login"
            })
    )
}