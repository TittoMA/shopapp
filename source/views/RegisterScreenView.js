import React, { Component } from 'react';
import { View, Text, Image, Dimensions, CheckBox } from 'react-native';
import Input from './components/Input';
import Button from './components/Button';
import { ScrollView } from 'react-native-gesture-handler';

export default class RegisterScreenView extends Component {
    constructor(props) {
        super(props);
        this.state = {
            
        };
    }

    render() {
        return <View style={{flex: 1, paddingHorizontal: 16, backgroundColor: "#EEEEEE",}}>
            <ScrollView showsVerticalScrollIndicator={false}>
            <View style={{alignItems: "center", marginBottom : 30, marginTop : 20}}>
                <Image
                    style ={{
                        resizeMode : "contain",
                        width : Dimensions.get("window").width / 2
                    }}
                    source = {require('../assets/images/logo.png')}
                />
            </View>
            

            <View>
                <Input 
                    placeholder = {"Nama"}
                    radius = {8}
                    backgroundColor = {"white"}
                />

                <Input 
                    placeholder = {"Email"}
                    radius = {8}
                    backgroundColor = {"white"}
                />

                <Input 
                    placeholder = {"Password"}
                    radius = {8}
                    backgroundColor = {"white"}
                    secureMode = {true}
                />

                <Input 
                    radius = {8}
                    placeholder = {"Alamat Lengkap"}
                    backgroundColor = {"white"}
                />

                <Input 
                    radius = {8}
                    placeholder = {"No. Telepon"}
                    backgroundColor = {"white"}
                />
            </View>

            <View style={{flexDirection : "row", alignItems: "center", marginBottom : 20}}>
                <CheckBox 
                    disabled={this.props.state.laki}
                    value ={this.props.state.laki}
                    onValueChange = {this.props.method.laki}
                />

                <Text style={{marginRight : 16}}>Laki-laki</Text>

                <CheckBox 
                    disabled={this.props.state.perempuan}
                    value ={this.props.state.perempuan}
                    onValueChange = {this.props.method.perempuan}
                />

                <Text>Perempuan</Text>
            </View>

            <View style={{marginBottom : 20}}>
                 <Button
                    backgroundColor = {"#5e9bff"}
                    radius = {8}
                    label = {"DAFTAR"}
                />
            </View>
           
           </ScrollView>
        </View>
    }
}
