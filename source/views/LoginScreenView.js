import React, { Component } from 'react';
import { Text, View, Dimensions, ScrollView } from 'react-native';
import { AppConstants } from "../systems/Constants";
import ImageView from './components/ImageView';
import Input from './components/Input';
import Button from './components/Button';

export default class LoginScreenView extends Component {
    constructor(props) {
        super(props);
        this.state = {
        };
    }

    render() {
        return <View style={{ flex: 1 }}>
            <ScrollView contentContainerStyle={{ flexGrow: 1 }}>
            <View style={{ flex: 1, justifyContent: 'center', backgroundColor: AppConstants.ActiveTheme.AppBackgroundColor, paddingHorizontal: 2 * AppConstants.ActiveTheme.AppObjectSpacing }}>
                <ImageView
                    imageSrc={require('../assets/images/logo.png')}
                    width={Dimensions.get('window').width / 2}
                    height={10 * AppConstants.ActiveTheme.AppObjectSpacing}
                    style={{ alignSelf: 'center', marginBottom: 5 * AppConstants.ActiveTheme.AppObjectSpacing }} />
                <Input
                    placeholder={'Email'}
                    radius={AppConstants.ActiveTheme.AppObjectSpacing}
                    keyboardType={'email-address'} />
                <Input
                    placeholder={'Password'}
                    radius={AppConstants.ActiveTheme.AppObjectSpacing}
                    secureMode={true} />
                <Button
                    height={AppConstants.ActiveTheme.AppInputHeightDefault + AppConstants.ActiveTheme.AppObjectSpacing}
                    label={'LOGIN'}
                    onPress={()=>this.props.onLoginPress()}
                    radius={AppConstants.ActiveTheme.AppObjectSpacing} />
                <View style={{ marginTop: 2 * AppConstants.ActiveTheme.AppObjectSpacing, alignItems: 'center' }}>
                    <Text 
                        onPress={()=>this.props.onForgotPress()}
                        style={{ fontSize: 16, color: AppConstants.ActiveTheme.AppFontBlackColor }}>
                        Lupa Password
                    </Text>
                    <View style={{ flexDirection: 'row', marginTop: 2 * AppConstants.ActiveTheme.AppObjectSpacing }}>
                        <Text style={{ fontSize: 16, color: AppConstants.ActiveTheme.AppFontBlackColor }}>
                            {'Belum Punya Akun?'}
                        </Text>
                        <Text 
                            onPress={()=>this.props.onRegisterPress()}
                            style={{ fontSize: 16, color: AppConstants.ActiveTheme.AppPrimaryColor }}>
                            {' Daftar Disini'}
                        </Text>
                    </View>
                </View>
            </View>
            </ScrollView>
        </View>
    }
}
