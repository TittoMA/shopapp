import React from 'react';
import { BackHandler } from 'react-native';

import LoginScreenView from "../views/LoginScreenView";

export default class LoginScreen extends React.Component {

    constructor(props) {

        super(props);

        this.state = {
            email: "",
            password: ""
        }

        this.content = [];
    }

    componentDidMount = () => {
        BackHandler.addEventListener('hardwareBackPress', this.handlerBack);
    }

    componentWillUnmount = () => {
        BackHandler.removeEventListener('hardwareBackPress', this.handlerBack);
    }

    _onBackPress = () => {
        BackHandler.removeEventListener('hardwareBackPress', this.handlerBack)
    }

    handlerBack = () => {
        BackHandler.exitApp()
        return true
    }

    loginHandler = async () => {
        this.props.navigation.push("Register");
    }

    onForgotPress = async () => {
        this.props.navigation.push("Register");
    }

    onRegisterPress = async () => {
        this.props.navigation.push("Register");
    }

    render = () => {
        return <LoginScreenView 
            onLoginPress={()=> this.loginHandler()}
            onRegisterPress={()=> this.onRegisterPress()}
            onForgotPress={()=> this.onForgotPress()} />
    }


}