import React from 'react';
import { ToastAndroid, BackHandler, AsyncStorage} from 'react-native';
import RegisterScreenView from "../views/RegisterScreenView";

export default class RegisterScreen extends React.Component {

    constructor(props) {

        super(props);

        this.state = {
            laki : true,
            perempuan : null,
            jk : "Laki-laki"
        }

        this.method = {
            perempuan: this._perempuan.bind(this),
            laki: this._laki.bind(this),
        }

    }

    componentDidMount() {}

    componentWillUnmount() { }

    componentWillMount = () => {
    }

    _perempuan = () =>{
        this.setState({laki : false, perempuan: true, jk: "Perempuan"})
    }

    _laki = () =>{
        this.setState({laki : true, perempuan: false, jk: "Laki-laki"})
    }
    render = () => {

        return <RegisterScreenView 
            state = {this.state}
            method = {this.method}
        />

    }


}